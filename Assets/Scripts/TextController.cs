﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {
	public Text userText           ;
	public Text startGame          ; 
	public Text developedBy        ;
	bool        gameStarted = false;
	
	/*
	 * State Options:
	 * Prior to game start: 
	 *     0 -> 10 - used to control screen
	 * After game start: 
	 *     0 - game has not started
	 *     1 - player in first level
	 *     2 - player in second level
	 *     3 - end of the game
	 */
	int         state       = 0    ;
	
	/*
	 * Location Options:
	 *     0 - Cell
	 *     1 - Sheets
	 *     2 - Mirror
	 *     3 - Cell Door
	 */
	int         location    = 0    ;
	
	void Start () 
	{
	}
	
	void Update () 
	{
		if(gameStarted == false)
		{
			Display_StartGame();
		}
		
		Get_Input();
	}		
	
	void Display_StartGame ()
	{
		if(state == 10)
		{
			state = 0;
			startGame.enabled = false;
		}
		else if(state == 2)
		{
			state ++;
			startGame.enabled = true;
		}
		else
		{
			state++;
		}	
	}
	
	void Display_Cell_0 ()
	{
		string msg = "    You have awoken inside a prison cell drenched in a pool of sweat. "
				+ "You can hear other inmates complaining that the AC unit has broken again. "
				+ "Still in the clothing that you wore last night, you are unsure as to how "
				+ "you actually got into this mess. As you raise yourself off of the disguntingly "
				+ "cold floor you notice a pile of moldy sheets in the corner. Would last night "
				+ "have been better or worse with those?"
				+ "\n"
				+ "    What the hell happened? You struggle to get fully upright, your legs unsteady "
				+ "as if you were vigorously excercising. Or perhaps you were running from "
				+ "something. You lose your balance but catch yourself on a rusty bar from the "
				+ "cell door in front of you. The cut itself does not look bad, but the wound "
				+ "is sure to become infected if not treated."
				+ "\n"
				+ "    You can see a broken mirror behind you, with shards of glass scattered on the "
				+ "floor. This is clearly someplace that does not worry about your safety. "
				+ "You need to leave here quickly. What should you do?";
				
		string[] options = {
			"Press [s] to view the sheets.",
			"Press [m] to view the mirror.",
			"Press [d] to view the cell door."
		};
		Display_ToUser(msg, options);
		
	}
	
	void Display_Sheets_0 ()
	{
		string msg = "    This pile of sheets appears to have been sitting here for quite some time. "
				+ "No doubt scaring away other residents from using them, could you use them for "
				+ "anything? The mold on the sheets could serve as an antibiotic of sorts, or it "
				+ "could guarantee that infection you worried about. Where is Gregory House when "
				+ "you need him?";
				
		string[] options = {
			"Press [c] to return to the middle of the cell.",
			"Press [m] to view the mirror."                 ,
			"Press [d] to view the cell door."
		};
		Display_ToUser(msg, options);
	}
	
	void Display_Sheets_1 ()
	{
		string msg = "    The same pile of sheets is here. Nothing has changed. Best to look elsewhere.";
		
		string[] options = {
			"Press [c] to return to the middle of the cell.",
			"Press [m] to view the mirror."                 ,
			"Press [d] to view the cell door."			
		};
		Display_ToUser(msg, options);
	}
	
	void Display_Mirror_0 ()
	{
		string msg = "    The mirror appears to have been purposefully broken. Perhaps a former inmate "
				+ "was searching for something inside. Behind the mirror is a concrete wall, so there "
				+ "is no escape opportunity there. Although one of the glass shards could come in handy."
				+ "Just make sure you do not cut yourself again.";
		
		string[] options = {
			"Press [space] to pick up a glass shard."       ,
			"Press [c] to return to the middle of the cell.",
			"Press [s] to view the sheets."                 ,
			"Press [d] to view the cell door."					
		};
		Display_ToUser(msg, options);
	}
	
	void Display_Mirror_0_Action ()
	{
		string msg = "     You carefully reach down and select a shard of glass, best to handle this "
				+ "with care so that you do not injure yourself further. You should probaby look "
				+ "elsewhere if you hope to escape this prison cell.";
		
		string[] options = {
			"Press [c] to return to the middle of the cell.",
			"Press [s] to view the sheets."                 ,
			"Press [d] to view the cell door."		
		};
		
		Display_ToUser(msg, options);
	}
	void Display_Mirror_1 ()
	{
		string msg = "    The broken mirror... I wonder if this thing has ever been cleaned. Nothing "
				+ "has changed. Best to look elsewhere.";
		
		string[] options = {
			"Press [c] to return to the middle of the cell.",
			"Press [s] to view the sheets."                 ,
			"Press [d] to view the cell door."					
		};
		Display_ToUser(msg, options);	
	}
	
	void Display_CellDoor_0 ()
	{
		string msg = "    This damn rusty door will probably cause you to have get Tetanus. You will "
				+ "need to be more careful when around this door. The years sure have taken a toll "
				+ "onto the lock mechanism, upon inspection the gears appear to be loose. Now if "
				+ "only you could find something to stick into the lock and move the mechanisms. "
				+ "Do you happen to know a good lockpick?";
		
		string[] options = {
			"Press [c] to return to the middle of the cell.",
			"Press [s] to view the sheets."                 ,
			"Press [m] to view the mirror."                 ,
		};
		Display_ToUser(msg, options);
	}
	
	void Display_CellDoor_1 ()
	{
		string msg = "    It may be possible to jam the broken shard into the mechanism, and by "
				+ "turning it the cell might open. Is freedom actually possible? Most likely you "
				+ "will end up screwing this up, and will only hurt yourself more. Perhaps you "
				+ "should just go take a nap on the moldy sheets.";
		
		string[] options = {
			"Press [space] to jam the glass shard into the lock, and attempt to turn it.",
			"Press [c] to return to the middle of the cell."                             ,
			"Press [s] to view the sheets."                                              ,
			"Press [m] to view the mirror."                                              ,		
		};
		Display_ToUser(msg, options);
	}
	
	void Display_EndGame()
	{
		string msg = "    Freedom!!! Congratulations your cell is open and you may now do as you "
				+ "please. Hope you enjoyed this little game.";
		
		userText.text       = msg ;
		developedBy.enabled = true;
	}
	
	void Display_ToUser (string msg, string[] options)
	{
		string output = msg + "\n";
		
		foreach (string option in options)
		{
			output += "\n" + option;
		}
		userText.text = output;
	}
	
	void Get_Input ()
	{
		if(Input.GetKeyDown(KeyCode.Space) && gameStarted == false)
		{
			state               = 0    ;
			gameStarted         = true ;
			startGame.enabled   = false;
			developedBy.enabled = false;
			Display_Cell_0()           ;
		}
		else if(Input.GetKeyDown(KeyCode.Space) && gameStarted == true)
		{
			if(location == 2 && state == 0)
			{
				state = 1                ;
				Display_Mirror_0_Action();
			}
			else if(location == 3 && state == 1)
			{
				state = 3        ;
				Display_EndGame();
			}
		}
		else if(Input.GetKeyDown(KeyCode.C) && gameStarted == true && state != 3)
		{
			location = 0    ;
			Display_Cell_0();
		}
		else if(Input.GetKeyDown(KeyCode.D) && gameStarted == true && state != 3)
		{
			location = 3;
			if(state == 0)
				Display_CellDoor_0();
			else if(state == 1)
				Display_CellDoor_1();
		}
		else if(Input.GetKeyDown(KeyCode.M) && gameStarted == true && state != 3)
		{
			location = 2;
			if(state == 0)
				Display_Mirror_0();
			else if(state == 1)
				Display_Mirror_1();
		}
		else if(Input.GetKeyDown(KeyCode.S) && gameStarted == true && state != 3)
		{
			location = 1;
			if(state == 0)
				Display_Sheets_0();
			else if(state == 1)
				Display_Sheets_1();
		}
	}
}
